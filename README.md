# Flask Annotator


Une aide à l'annotation grammaticale sous la forme d'une application Flask. 

## Installation et mise en place


    git clone https://gitlab.huma-num.fr/mgillelevenson/flask_annotator
    cd flask_annotator
    python3 -m venv mon_env
    source mon_env/bin/activate
    pip3 install -r requirements.txt


## Utilisation 

Après installation de Freeling et de Pie, ``python3 app.py`` 
suffit à lancer le programme. 

## Possibilités

Annotation grammatical du castillan médiéval, 
classique et moderne (en cours de développement), 
ainsi que du latin classique. Il est pour 
l'instant fonctionnel uniquement avec le castillan moderne.

En entrée, sont acceptés: 
- le format texte
- le format XML/TEI

En sortie: 
- le format texte
- le format XML/TEI
- le format tsv
- le format XML/W (facilite l'importation du 
corpus dans des outils comme TXM)

Certaines combinaisons ne sont pas possibles (txt > XML/TEI par exemple)

## Modèles disponibles

Sont disponibles pour l'instant un modèle d'analyse du castillan moderne (textes du XIXe siècle essentiellement) ainsi que
le modèle crée par Thibault Clérice à partir des données du Lasla (latin classique essentiellement, voir plus bas). 

## Dépendances

Ce projet s'inscrit dans la continuité de mon travail sur l'annotation
de sources XML-TEI: voir [ici](https://gitlab.huma-num.fr/mgillelevenson/lemmatisation_xml_tei).


Les outils d'annotation ne sont évidemment pas les miens. 
J'utilise deux outils principaux:

- Freeling
- Pie

Le modèle de lemmatisation du latin médiéval a été créé par Thibault Clérice (voir plus bas).

## Jeu d'étiquettes

Les jeux d'étiquettes utilisés sont les suivants: 
- CATTEX pour le modèle LASLA;
- EAGLES pour le castillan médiéval. Attention, EAGLES propose un jeu d'étiquettes qui fusionne parties du discours et morphologie. 





## Projets similaires

Ce projet, comme son projet d'origine, a une dette intellectuelle importante envers le projet [Falcon](https://github.com/CondorCompPhil/falcon) (Jean-Baptiste Camps, Lucence Ing et Elena Spadini), 
notamment pour la possibilité de lemmatiser avec plusieurs outils, freeling ou pie. 

## Bibliographie



* Jean-Baptiste Camps, Elena Spadini. CondorCompPhil/falcon: [https://github.com/CondorCompPhil/falcon](https://github.com/CondorCompPhil/falcon).


Le modèle de lemmatisation présent sur le dépôt est celui entraîné par Thibault Clérice (ÉnC) sur les données du LASLA
([ici](https://github.com/chartes/deucalion-model-lasla)):
*   Thibault Clérice. (2019, February 1). chartes/deucalion-model-lasla: LASLA Latin Lemmatizer - Alpha (Version 0.0.1). 
Zenodo. http://doi.org/10.5281/zenodo.2554847 _Check the latest version here:_[Zenodo DOI](https://doi.org/10.5281/zenodo.2554846)

Freeling:
* Lluís Padró and Evgeny Stanilovsky. *FreeLing 3.0: Towards Wider Multilinguality*. Proceedings of the Language Resources and Evaluation Conference (LREC 2012) ELRA. Istanbul, Turkey. May, 2012.


* Ariane Pinche (2019). Annoter facilement un corpus complexe : l'exemple de Pyrrha, interface de post-correction, et Pie, 
lemmatiseur et tagueur morphosyntaxique, pour l'ancien français. Rencontres lyonnaises des jeunes chercheurs en linguistique historique, Lyon. 
[https://hal.archives-ouvertes.fr/hal-02151796](https://hal.archives-ouvertes.fr/hal-02151796)

CLTK:
* Kyle P. Johnson et al.. (2014-2019). CLTK: The Classical Language Toolkit. DOI 10.5281/zenodo.3445585

