#! /usr/bin/python
# -*- coding:utf-8 -*-
import shutil
from flask import Flask, render_template, request, send_file, make_response, send_from_directory, redirect
from werkzeug.utils import secure_filename
import configuration
import annotator

app = Flask(__name__)
app.debug = False  # a supprimer avec la version de production



@app.route('/')
def redirection():
    return redirect("/Annotator", code=302)

@app.route('/Annotator', methods=['GET', 'POST'])
def accueil():
    """
    Page principale, l'outil d'annotation. Pour l'instant, seule l'annotation de corpus au format texte est possible.
    :return:
    """
    if request.method == 'GET':  # arrivée sur la page d'accueil avec méthode get
        return render_template('accueil.html')
    else:  # si la méthode est post, c'est qu'on a rempli et envoyé le formulaire.
        options = configuration.creation_options()
        reponse = verif_fichier()
        if reponse[0] is True:
            nom_fichier = reponse[1]
            chemin_fichier_entree = 'uploads/%s' % nom_fichier
            if options["langue"] == 'cast_moderne':
                annotator.lemmatisation_cast_moderne(chemin_fichier_entree, options["lemmes"], options["pos"],
                                                     options["morph"])
                output = annotator.production_document_de_sortie(nom_fichier, options["sortie"], options["lemmes"],
                                                                 options["pos"],
                                                                 options["morph"])
            if options["langue"] == 'latin_medieval':
                annotator.principal(chemin_fichier_entree, options["langue"])
            return render_template('annotation.html', nom_fichier=nom_fichier)
        else:
            return render_template('error.html', erreur=reponse[0])


@app.route('/Annotator/resultat/<nom_fichier>')  # chemin vers les fichiers de sortie
def return_file(nom_fichier):
    return send_from_directory("result/", nom_fichier, as_attachment=True)


@app.route('/Annotator/credits')
def credits():
    return render_template('credits.html')


@app.route('/Annotator/a-propos')
def aPropos():
    return render_template('a-propos.html')


def verif_fichier():
    """
    Fonction de vérification du fichier à téléverser. Faut-il plutôt utiliser les exceptions ?
    :return: Une liste avec un message d'erreur (ou pas) et le nom du fichier
    """
    fichier = request.files['mon_fichier']
    if not fichier:
        reponse = ['pas de fichier sélectionné']  # a la place, renvoyer vers la page d accueil avec un message d
        # erreur
        return reponse
    else:
        nom_fichier = fichier.filename
        if nom_fichier.split('.')[1] in ['txt', 'xml']:
            nom_fichier = secure_filename(nom_fichier)
            fichier.save('./uploads/' + nom_fichier)
            reponse = [True, nom_fichier]
        else:
            reponse = ['format incorrect', nom_fichier]
        return reponse


if __name__ == '__main__':
    app.run(host='0.0.0.0')
