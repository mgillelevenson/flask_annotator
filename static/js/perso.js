$( document ).ready(function() {
    $("#morph").prop('disabled', true);
    $("#cast_classique").prop('disabled', true);
    $("#cast_medieval").prop('disabled', true);
    $("#latin_classique").prop('disabled', true);
    $("#latin_medieval").prop('disabled', true);
    $("#sortie_tei").prop('disabled', true);
});

var check_txt = $("#entree_txt");
var check_tei = $("#entree_tei");
$("#entree_txt").on('click',checkStatusTEI);
$("#entree_txt").on('click',checkStatusTEI2);
$("#entree_tei").on('click',checkStatusTEI);
$("#entree_tei").on('click',checkStatusTEI2);

function checkStatusTEI(){
  if(check_txt.is(':checked'))
    {
       $("#cast_moderne").prop('disabled', false);
       $("#latin_medieval").prop('disabled', true);
       $("#sortie_tei").prop('disabled', true);
       document.getElementById("sortie_XML-W").checked = true;
       document.getElementById("cast_moderne").checked = true;
     }
 else{
       $("#sortie_tei").prop('disabled', false);
       $("#cast_moderne").prop('disabled', true);
       $("#latin_medieval").prop('disabled', false);
    }

 }
 
function checkStatusTEI2(){
  if(check_tei.is(':checked'))
    {
        document.getElementById("sortie_tei").checked = true;
       $("#sortie_XML-W").prop('disabled', true);
       $("#sortie_tsv").prop('disabled', true);
       $("#cast_moderne").prop('disabled', true);
       $("#sortie_txt").prop('disabled', true);
       document.getElementById("latin_medieval").checked = true;
     }
 else{
       $("#sortie_XML-W").prop('disabled', false);
       $("#sortie_tsv").prop('disabled', false);
       $("#cast_moderne").prop('disabled', false);
       $("#sortie_txt").prop('disabled', false);
    }
}

var check1 = $("#cast_medieval");
var check2 = $("#cast_classique");
var check3 = $("#cast_moderne");
//$("#cast_medieval").on('click',checkStatusTEI);
//$("#cast_classique").on('click',checkStatusTEI);
//$("#cast_moderne").on('click',checkStatusTEI);
//$("#latin_classique").on('click',checkStatusTEI);
//$("#latin_medieval").on('click',checkStatusTEI);
 
 function checkStatusMorph(){
  if(check1.is(':checked') || check2.is(':checked') || check3.is(':checked'))
    {
       $("#morph").prop('disabled', true);
     }
 else{
       $("#morph").prop('disabled', false);
    }

 }
 
 