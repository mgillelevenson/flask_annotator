from flask import request


def creation_options():
    """
    Chargement des résultats du formulaire
    :return: un dictionnaire
    """
    entree = request.form['entree']
    sortie = request.form['sortie']
    langue = request.form['langue']
    if not request.form.get('annotation_lemmes'):
        lemmes = False
    else:
        lemmes = True

    if not request.form.get('annotation_pos'):
        pos = False
    else:
        pos = True

    if not request.form.get('annotation_morph'):
        morph = False
    else:
        morph = True
    return {
        "lemmes": lemmes,
        "pos": pos,
        "morph": morph,
        "entree": entree,
        "sortie": sortie,
        "langue": langue
    }
